import sys
import signal

from gi import require_version
require_version('Gtk', '4.0')
from gi.repository import GLib, GObject, Gio, Gtk
signal.signal(signal.SIGINT, signal.SIG_DFL)

factory_text = """
<?xml version='1.0' encoding='UTF-8'?>
<interface>
  <template class='GtkListItem'>
    <property name='child'>
      <object class='GtkLabel'>
        <property name='ellipsize'>end</property>
        <property name='xalign'>0</property>
        <binding name='label'>
          <lookup name='string' type='GtkStringObject'>
            <lookup name='item'>GtkListItem</lookup>
          </lookup>
        </binding>
      </object>
    </property>
  </template>
</interface>
""" 

def read_lines_cb (stream,
                   result,
                   stringlist):
    pass
#   GError *error = NULL;
#   gsize size;
#   GPtrArray *lines;
#   gssize n_filled;
#   const char *buffer, *newline;
# 
#   n_filled = g_buffered_input_stream_fill_finish (stream, result, &error);
#   if (n_filled < 0)
#     {
#       g_print ("Could not read data: %s\n", error->message);
#       g_clear_error (&error);
#       g_object_unref (stringlist);
#       return;
#     }
# 
#   buffer = g_buffered_input_stream_peek_buffer (stream, &size);
# 
#   if (n_filled == 0)
#     {
#       if (size)
#         gtk_string_list_take (stringlist, g_utf8_make_valid (buffer, size));
#       g_object_unref (stringlist);
#       return;
#     }
# 
#   lines = NULL;
#   while ((newline = memchr (buffer, '\n', size)))
#     {
#       if (newline > buffer)
#         {
#           if (lines == NULL)
#             lines = g_ptr_array_new_with_free_func (g_free);
#           g_ptr_array_add (lines, g_utf8_make_valid (buffer, newline - buffer));
#         }
#       if (g_input_stream_skip (G_INPUT_STREAM (stream), newline - buffer + 1, NULL, &error) < 0)
#         {
#           g_clear_error (&error);
#           break;
#         }
#       buffer = g_buffered_input_stream_peek_buffer (stream, &size);
#     }
#   if (lines == NULL)
#     {
#       g_buffered_input_stream_set_buffer_size (stream, g_buffered_input_stream_get_buffer_size (stream) + 4096);
#     }
#   else
#     {
#       g_ptr_array_add (lines, NULL);
#       gtk_string_list_splice (stringlist, g_list_model_get_n_items (G_LIST_MODEL (stringlist)), 0, (const char **) lines->pdata);
#       g_ptr_array_free (lines, TRUE);
#     }
# 
#   g_buffered_input_stream_fill_async (stream, -1, G_PRIORITY_HIGH_IDLE, NULL, read_lines_cb, data);
# }

def file_is_open_cb (file,
                     result,
                     data):

    file_stream = file.read_finish(result)

    print(file_stream)
    if not file_stream:
        print("Could not open file: {}\n") #.format(error.message)
        
        #TODO: get to understand if I need the following
        # g_error_free (error);
        # g_object_unref (data);

    stream = Gio.BufferedInputStream.new(file_stream)
    stream.fill_async (-1, GObject.PRIORITY_DEFAULT_IDLE, None, read_lines_cb, data)

def load_file (stringlist,
               file):

    string_list.splice(0, stringlist.get_n_items())
    file.read_async (GObject.PRIORITY_DEFAULT_IDLE, None, file_is_open_cb, stringlist);

def open_response_cb (dialog,
                      response,
                      stringlist):
    dialog.hide()

    if response == Gtk.ResponseType.ACCEPT:
        file = dialog.get_file() 
        load_file (stringlist, file);
        
        #TODO: check g_object_unref python-equiv
        del file

    dialog.destroy()

def file_open_cb (button,
                  stringlist):

    dialog = Gtk.FileChooserNative.new("Open file",
                                       button.get_root(),
                                       Gtk.FileChooserAction.OPEN,
                                       "_Load",
                                       "_Cancel")
    dialog.set_modal(True)
    dialog.connect("response", open_response_cb, stringlist)
    dialog.show()

class ListViewWords(Gtk.ApplicationWindow):
    
  def __init__(self, **kwargs): 
    super().__init__(**kwargs)

    file = Gio.File.new_for_path("/usr/share/dict/words")

    if file.query_exists():
        stringlist = Gtk.StringList.new()
        load_file(stringlist, file)
    else:
        words = "lorem ipsum dolor sit amet consectetur adipisci elit sed eiusmod tempor incidunt labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat".split(" ")
        stringlist = Gtk.StringList.new(words)
    
    #TODO: check g_object_unref python-equiv
    del file

    expression = None
#    expression = Gtk.PropertyExpression.new(GObject.TYPE_GSTRING, None, "string")
    # expression = Gtk.PropertyExpression.new(Gtk.StringObject, None, "string")

    filter = Gtk.StringFilter.new(expression)
    filter_model = Gtk.FilterListModel.new(stringlist, filter)
    filter_model.set_incremental(True)

    self.set_default_size(400, 600)

    header = Gtk.HeaderBar()
    header.set_show_title_buttons(True)
    open_button = Gtk.Button.new_with_mnemonic("_Open")
    open_button.connect("clicked", file_open_cb, stringlist)
    header.pack_start(open_button)
    self.set_titlebar(header)

    # If launched from another program, show on same display
    # self.set_display(do_widget.get_display())

    #TODO: why is this needed?
    # g_object_add_weak_pointer (G_OBJECT (self), (gpointer*)&window);

    vbox = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
    self.set_child(vbox)

    search_entry = Gtk.SearchEntry()
    search_entry.bind_property("text", filter, "search", 0)
    vbox.append(search_entry)

    overlay = Gtk.Overlay()
    vbox.append(overlay)

    progress = Gtk.ProgressBar()
    progress.set_halign(Gtk.Align.FILL)
    progress.set_valign(Gtk.Align.START);
    progress.set_hexpand(True)
    overlay.add_overlay(progress);

    sw = Gtk.ScrolledWindow()
    sw.set_vexpand(True)
    overlay.set_child(sw)

    list_item_factory = Gtk.BuilderListItemFactory.new_from_bytes(None,
                            GLib.Bytes(bytes(factory_text, "UTF-8")))

    no_selection = Gtk.NoSelection.new(filter_model)

    listview = Gtk.ListView.new (no_selection, 
                                 list_item_factory)
    sw.set_child(listview)

    filter_model.connect("items-changed", self.update_title_cb, progress)
    filter_model.connect("notify::pending", self.update_title_cb, progress)
    self.update_title_cb(filter_model, 0, 34, 0, progress)

  def update_title_cb (self, model, which, key, dontknow, progress):
  
    print("self", self)
    print("model", model)
    print("progress", progress)

    total = model.get_n_items()
    pending = model.get_pending()
    title = "{} lines".format(total)
    
    progress.set_visible(True)
    if total:
      progress.set_fraction( (total - pending) / total )

    self.set_title(title)


class Application(Gtk.Application):
    def __init__(self):
        super().__init__(application_id="org.gnome.words",
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = ListViewWords(application=self)
        win.present()

def main():
    app = Application()
    return app.run(sys.argv)

if __name__ == "__main__":
    sys.exit(main())
